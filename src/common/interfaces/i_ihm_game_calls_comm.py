from abc import ABC, abstractmethod
from uuid import UUID
from data_structures.message import Message
from data_structures.move import Move


class I_IHMMainCallsComm(ABC):
    @abstractmethod
    def send_message(self, message: Message):
        pass

    @abstractmethod
    def place_tile(self, move: Move):
        pass

    @abstractmethod
    def quit_spectator_interface(self, user_id: UUID, game_id: UUID):
        pass
