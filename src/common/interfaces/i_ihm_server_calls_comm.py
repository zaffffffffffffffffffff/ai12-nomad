from abc import ABC, abstractmethod


class I_IHMServerCallsComm(ABC):
    @abstractmethod
    def start_server(self, ip: str, port: int):
        pass

    @abstractmethod
    def quit_server(self):
        pass
