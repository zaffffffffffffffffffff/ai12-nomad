import uuid as uuid_lib
from profile import Player


class Move:
    """
    "abstract" (cough) data class for nomad games moves, to be sent thru the network.
    At init : for a random message_uuid, don't set a uuid value, or set it at 0/false
    """

    def __init__(
        self,
        game_uuid: uuid_lib.UUID,
        player: Player,
        move_uuid: uuid_lib.UUID = uuid_lib.uuid4(),
    ):
        self.game_uuid = game_uuid
        self.player = player


class MoveSkip(Move):
    """
    Empty move.

    At init : for a random message_uuid, don't set a uuid value, or set it at 0/false
    """

    pass


class MoveTile(Move):
    """
    Tile placing move.

    At init : for a random message_uuid, don't set a uuid value, or set it at 0/false
    """

    def __init__(
        self,
        game_uuid: uuid_lib.UUID,
        player: Player,
        row: int,
        column: int,
        move_uuid: uuid_lib.UUID = uuid_lib.uuid4(),
    ) -> None:
        super().__init__(game_uuid, player, move_uuid)
        self.row = row
        self.column = column


class MoveTower(MoveTile):
    """
    Tile placing move.

    At init : for a random message_uuid, don't set a uuid value, or set it at 0/false
    """

    pass


if __name__ == "__main__":
    u = uuid_lib.uuid4()
    print(u)
    p = Player("Joris", u)
    m = MoveSkip(u, p)
    print(m)
